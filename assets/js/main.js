/**
 * main.js
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2014, Codrops
 * http://www.codrops.com
 */
(function() {

	var bodyEl = document.body,
		content = document.querySelector( '.content-wrap' ),
		// content2 = document.querySelector( '.content-wrap2' ),
		openbtn = document.getElementById( 'open-button' ),
		openbtn2 = document.getElementById( 'open-button2' ),
		closebtn = document.getElementById( 'close-button' ),
		isOpen = false;

	function init() {
		initEvents();
	}

	function initEvents() {
		openbtn.addEventListener( 'click', toggleMenu );
		if( closebtn ) {
			closebtn.addEventListener( 'click', toggleMenu );
		}

		// close the menu element if the target it´s not the menu element or one of its descendants..
		content.addEventListener( 'click', function(ev) {
			var target = ev.target;
			if( isOpen && target !== openbtn ) {
				toggleMenu();
			}
		} );
		
		openbtn2.addEventListener( 'click', toggleMenu2 );
		if( closebtn ) {
			closebtn.addEventListener( 'click', toggleMenu2 );
		}

		// close the menu element if the target it´s not the menu element or one of its descendants..
		// content2.addEventListener( 'click', function(ev) {
		// 	var target = ev.target;
		// 	if( isOpen && target !== openbtn2 ) {
		// 		toggleMenu2();
		// 	}
		// } );
	}

	function toggleMenu() {
		if( isOpen ) {
			classie.remove( bodyEl, 'show-menu' );
		}
		else {
			classie.add( bodyEl, 'show-menu' );
		}
		isOpen = !isOpen;
	}
	
	function toggleMenu2() {
		if( isOpen ) {
			classie.remove( bodyEl, 'show-menu2' );
		}
		else {
			classie.add( bodyEl, 'show-menu2' );
		}
		isOpen = !isOpen;
	}

	init();

})();