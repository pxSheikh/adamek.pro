# Project Name: adamek.pro website

## Introduction

**adamek.pro** serves as a personal portfolio that showcases my skills, experiences, and completed projects. Designed to present my graphic design work in a visually appealing and responsive manner, it provides an insight into my professional journey, highlighting my completed internships, studies, and a variety of graphic projects.

## Project Overview

- **Inception Year:** 2016
- **Type:** One-Page Portfolio Website
- **URL:** [adamek.pro](https://v1.adamek.design)

## About the Project

_Between 2016 and 2018, I created and maintained_ **adamek.pro** _to present my portfolio in a concise and engaging format. This website offers a comprehensive view of my background, including my skills, completed internships, and academic achievements. The core of the site is dedicated to showcasing my graphic design projects, such as visual identities, posters, illustrations, publications, and websites. The website features a contact form built with PHP, allowing visitors to easily get in touch with me._

## Tech Stack

- HTML
- PHP
- CSS
- JavaScript
