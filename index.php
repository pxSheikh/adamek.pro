﻿<?php
	if (isset($_POST["submit"])) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$message = $_POST['message'];
		$subject = $_POST['subject'];
		$human = intval($_POST['human']);
		$nag = '[adamek.pro] NEW MSG'; 
		$to = 'konrad_a@wp.pl'; 
		
		$body ="From: $name\n E-Mail: $email\n Message:\n $message";
		// Check if name has been entered
		if (!$_POST['name']) {
			$errName = 'Please enter your name';
		}
		// Check if email has been entered and is valid
		if (!$_POST['email'] || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			$errEmail = 'Please enter a valid email address';
		}
		// Check if subject has been entered
		if (!$_POST['subject']) {
			$errSubject = 'Please enter any subject';
		}
		//Check if message has been entered
		if (!$_POST['message']) {
			$errMessage = 'Please enter your message';
		}
		//Check if simple anti-bot test is correct
		if ($human !== 5) {
			$errHuman = 'Your answer is incorrect';
		}
		// If there are no errors, send the email
		if (!$errName && !$errEmail && !$errSubject && !$errMessage && !$errHuman) {
			if (mail ($to, $nag, $body, $subject)) {
				$result='<div class="alert alert-success">Thank You! I will answer as soon as possible.</div>';
			} else {
				$result='<div class="alert alert-danger">Sorry there was an error sending your message. Please try again later.</div>';
			}
		}
	}
?>


<!DOCTYPE html>
<html lang="en">
	<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <meta http-equiv="last-modified" content="Wed, 5 Sep 2018 01:46:00 GMT" />
    <meta http-equiv="content-language" content="en, pl" />
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Konrad Adamek - Graphic Designer - Portfolio & CV. Profession: Graphic Designer, Webmaster, Front-End Developer. In need of Graphic Design / WebSite / Online Store / Positioning of www? Check me out!">
    <meta name="author" content="adamek.pro">
    <meta name="keywords" content="Konrad, Adamek, Portfolio, CV, Graphic, Designer, Front-end, Developer, Website, Webpage, WWW, Positioning, Project, Projects, Logo, Poster, Posters, Design, Layout, Mockup, Visualisation, Identification, Visual, Business, Card, Cards, Photomontage, Photo, Manipulation, Photoshop, Raster, Vector, Graphics, HTML, CSS, Projektant, Graficzny, Grafik, Krakow, Cracow, Katowice, Grafika, Projektowanie, Graficzne, Projekty, Online, Store, Shop">
    <meta name="robots" content="index, follow">
    <meta name="googlebot" content="index, follow" />
    <meta name="copyright" content="© 2018 Konrad Adamek adamek.pro" />
    
    <link rel="alternate" hreflang="pl" href="pl" />
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <title>adamek.pro</title>
    
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!-- MAIN CSS -->
    <link href="assets/css/stylesheet.css" rel="stylesheet" />
    <!-- Animations -->
    <link href="assets/css/animate.css" rel="stylesheet" />
    <!-- Font icons -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/icons/Linea/styles.css" />
    <link rel="stylesheet" href="assets/icons/Elegant/style.css" />
    <!-- Portfolio CSS -->
    <link href="assets/css/magnific-popup.css" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126655791-1"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());
  		gtag('config', 'UA-126655791-1');
	</script>
	</head>

<body data-spy="scroll" data-target=".navigation">
	<div class="page-wrapper">
	
    <!-- Preloader -->
    <div id="loader">
    	<div id="loadercentral">
        	<div id="loaderInner"></div>
        </div>
	</div>
	
    <!-- Navigation -->
	<div class="menu-wrap">
		<nav class="menu navigation">
			<ul class="navbar menu-nav">
				<li><a class="easing" href="#sectionIntro">Intro</a></li>
            	<li><a class="easing" href="#sectionAbout">About</a></li>
            	<li><a class="easing" href="#sectionResume">Resume</a></li>
            	<li><a class="easing" href="#sectionWorks">Portfolio</a></li>
            	<li><a class="easing" href="#sectionContact">Contact</a></li>                        
			</ul>           
		</nav>
	</div>
	<button class="menu-button" id="open-button"><span aria-hidden="true" class="icon_menu"></span></button>
     
     <!-- Language --> 
	<div class="menu-wrap2">
		<nav class="language menu">
			<ul class="navbar menu-nav2">
            	<li><a id="active_lang" class="easing" href="#sectionIntro">English</a></li>
            	<li><a href="#">Polski</a></li>            
			</ul>           
		</nav>
	</div>
	<button class="menu-button2" id="open-button2"><span aria-hidden="true" class="icon_cogs"></span></button>

	<div class="content-wrap">
		<div class="content">

			<!--section INTRO-->
    		<section id="sectionIntro">
    			<div class="container-fluid">
      				<div class="container aboutContainer">

        				<!--INTRO Background -->
       					<div class="col-md-12 introHeader">
							<div class="introContent">
            					<div class="introHeading">
            						<h1 class="wow fadeinDown">Konrad Adamek</h1>
            						<div class="introTags wow zoomIn">GRAPHIC DESIGNER</div>
            					</div>
							</div>
          					<div class="introMouse">
           						<a class="easing scroll-down" href="#sectionAbout">
           							<div class="icon-basic-magic-mouse"></div>
           						</a>
           					</div>
						</div>
					
                    </div>
      			</div>
    		</section>
            
		</div>

		<!-- section ABOUT -->
    	<section id="sectionAbout">
			<div class="container">

        		<div class="sectionTitle wow fadeInUp">
        			<h1>About me</h1>
        			<h4>Let me introduce myself</h4>
        			<hr>
        		</div>
			
            <div class="col-md-4 aboutMe aboutStory wow fadeInUp">
				<h1>who am I</h1>
				<p>
					<span class="textGold">
						Hello, thank you for visiting. I'm Konrad Adamek, a creative Graphic Designer located in Katowice, Poland.
					</span>
					<br><br>
					I specialize in crafting captivating Logos, Posters, Billboards, Citylights, Business Cards, Brochures, Photomontages, 
					and various other design elements. Additionally, I enjoy creating visually appealing Magazines, Books, and Websites. 
					Don't forget to explore my Portfolio section. If you're interested in collaborating, feel free to reach out.
				</p>
			</div>
			
            <div class="col-md-4 aboutMe aboutProfile wow fadeInUp">
				<h1>Profile</h1>
				<img class="profileAv" src="assets/images/avatar.jpg" alt="">
				<h3>KONRAD ADAMEK</h3>
				<h4>GRAPHIC DESIGNER</h4>
				<div class="profileInfo">
					<p>#creative<br>#minimalistic</p>
					<h5>
                    	<span class="textGold">Birthday:</span>
						19 FEB 1994
					</h5>
					<h5>
						<span class="textGold">City:</span>
						Katowice, Poland
					</h5>
					<h5>
						<span class="textGold">Website:</span>
						www.adamek.pro
					</h5>
					<h5>
						<span class="textGold">E-mail:</span>
						KONRAD@ADAMEK.PRO
					</h5>
				</div>
			</div>

			<div class="col-md-4 aboutMe aboutSkills wow fadeInUp">
				<h1>Skills</h1>
				<h4>Visual identifications</h4>
				<div class="progress">
					<div class="progress-bar" role="progressbar" aria-valuenow="94" aria-valuemin="0" aria-valuemax="100" style="width: 94%;">94%</div>
				</div>
				<h4>Websites & Webshops</h4>
				<div class="progress">
					<div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%;">85%</div>
				</div>    
				<h4>Publications</h4>
				<div class="progress">
					<div class="progress-bar" role="progressbar" aria-valuenow="97" aria-valuemin="0" aria-valuemax="100" style="width: 97%;">97%</div>
				</div>               
				<h4>ADS</h4>
				<div class="progress">
					<div class="progress-bar" role="progressbar" aria-valuenow="92" aria-valuemin="0" aria-valuemax="100" style="width: 92%;">92%</div>
				</div>
			</div>

		</div>
	</section>

	<!--section RESUME-->
    <section id="sectionResume">
		<div class="container">
      
        	<div class="sectionTitle wow fadeInUp">
        		<h1>Resume</h1>
        		<h4>More about my past</h4>
        		<hr>
        	</div>

			<div class="col-md-6 resumeEe resumeEd wow fadeInUp">
				<ul class="resumeList">
					<li><h3>Education</h3></li>
                    <li>
      					<div class="icon icon-basic-notebook"></div>
      					<h4>      
      						<i class="resumePeriod">2010 - 2014</i>      
      						UPPER - SECONDARY SCHOOL OF DATA and COMMUNICATION 
      					</h4>
      					<div class="resumeName">profession: graphic designer</div>
      					<p>I have passed IT Specialist exam.</p>
      				</li>
					<li>
      					<div class="icon icon-basic-notebook"></div>
      					<h4>      
      						<i class="resumePeriod">2014 - 2018</i>      
      						HIGH SCHOOL OF IT TECHNOLOGY
                            <div style="visibility:hidden">.</div>
      					</h4>
      					<div class="resumeName">profession: graphic designer</div>
      					<p>I have completed my Bachelor's Degree.</p>
      				</li>
					<li>
      					<div class="icon icon-basic-notebook"></div>
      					<h4>      
      						<i class="resumePeriod">2018 - NOW</i>      
      						CRACOW ACADEMY OF FINE ARTS
                            <div style="visibility:hidden">.</div>
      					</h4>
      					<div class="resumeName">profession: graphic designer</div>
      					<p>Master's Degree Studies.</p>
      				</li>
				</ul>
			</div>

      		<div class="col-md-6 resumeEe resumeEm wow fadeInUp">
				<ul class="resumeList">
					<li><h3>Employment</h3></li>
					<li>
      					<div class="icon icon-basic-case"></div>
      					<h4>         
      						"Avalon" ADVERTISING AGENCY
      						<i class="resumePeriod">2012</i>
      						<div style="visibility:hidden">.</div>        
      					</h4>
      					<div class="resumeName">profession: graphic designer</div>
      					<p>Apprenticeships during Upper - Secondary School.</p>
      				</li>
					<li>
      					<div class="icon icon-basic-case"></div>
      					<h4>          
      						"KAMO Studio" CREATIVE AGENCY
      						<i class="resumePeriod">2016</i>
                            <div style="visibility:hidden">.</div>        
      					</h4>
      					<div class="resumeName">profession: graphic designer</div>
      					<p>Apprenticeships during High School.</p>
      				</li>
					<li>
      					<div class="icon icon-basic-case"></div>
      					<h4>          
      						"adamek.pro" SELF-EMPLOYED
      						<i class="resumePeriod">NOW</i>
                            <div style="visibility:hidden">.</div>        
      					</h4>
      					<div class="resumeName">profession: graphic designer, coder</div>
      					<p>Graphic Designer, Websites & Webshops & iOS Apps Coder</p>
      				</li>
				</ul>
			</div>

		</div>
	</section>
	
    <!-- section PORTOFOLIO -->
    <section id="sectionWorks">
		<div class="container-fluid">

        	<div class="sectionTitle wow fadeInUp">
        		<h1>Portfolio</h1>
        		<h4>Some of my projects</h4>
        		<hr>
			</div>

			<div id="worksGrid">
	            <div class="worksFiltersDiv wow fadeInUp">  
					<ul class="worksFilters">
                		<li class="filter" data-filter="all">All</li>
                		<li class="filter" data-filter=".worksCateg1">Visual identifications</li>
                		<li class="filter" data-filter=".worksCateg2">Posters</li>
                		<li class="filter" data-filter=".worksCateg3">Publications</li>
						<li class="filter" data-filter=".worksCateg4">Editorial graphics</li>
						<li class="filter" data-filter=".worksCateg5">Websites</li>
              		</ul>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12 worksItem worksCateg1">
					<img src="assets/images/works/ustron-iv.jpg" alt="Logo Identyfikacja Wizualna Ustron Miasto Uzdrowisko">
					<div class="itemCaption">
						<div class="capContent">
							<h3>USTROŃ</h3>
							<p>Visual identification</p>
							<a class="capPreview image-modal" href="#image-modal1">preview</a>
						</div>
					</div>  
					<div id="image-modal1" class="mfp-hide modal-box">
                		<img src="assets/images/works/ustron-iv.jpg" alt="Logo Identyfikacja Wizualna Ustron Miasto Uzdrowisko">
                  		<div class="modal-box-content">
                    		<h2>USTROŃ</h2>
                    		<p>Identyfikacja wizualna przygotowana dla miasta Ustroń w ramach konkursu na nowe logo uzdrowiska. Celem projektu było <br/> 
								stworzenie znaku zawierającego w sobie takie elementy jak kropla, góry czy litera „u” (ukryta pod postacią warstwy śnieżnej). <br/> 
								W ramach prezentacji, logo zostało zakomponowane w jego przykładowych zastosowaniach.</p>
                  		</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 worksItem worksCateg1">
					<img src="assets/images/works/mk-iv.jpg" alt="Logo Okladka Koszulki Bilety Identyfikacja Wizualna Mark Knopfler Tracker">
					<div class="itemCaption">
						<div class="capContent">
							<h3>MARK KNOPFLER "TRACKER"</h3>
							<p>Visual identification</p>
							<a class="capPreview image-modal" href="#image-modal2">preview</a>
						</div>
					</div>  
					<div id="image-modal2" class="mfp-hide modal-box">
                		<img src="assets/images/works/mk-iv.jpg" alt="Logo Okladka Koszulki Bilety Identyfikacja Wizualna Mark Knopfler Tracker">
                  		<div class="modal-box-content">
                    		<h2>MARK KNOPFLER "TRACKER"</h2>
                    		<p>Identyfikacja wizualna zaprojektowana dla Marka Knopflera - brytyjskiego gitarzysty, kompozytora i wokalisty, 
							   znanego przede wszystkim jako lidera legendarnego zespołu „Dire Straits”. W jej skład wchodzą: logo Mark Knopfler, 
							   bilety na koncert, okładka płyty oraz koszulki przeznaczone do sprzedaży w trakcie imprez. Założeniem projektów była 
							   dominacja wartości typograficznych, stylistyczna spójność materiału oraz czysty, minimalistyczny design.</p>
                  		</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 worksItem worksCateg1">
					<img src="assets/images/works/fhup-iv.jpg" alt="Logo Wizytowki Identyfikacja Wizualna Adamek Mariusz Hurtownia Pieczarek Pieczarki">
					<div class="itemCaption">
						<div class="capContent">
							<h3>F.H.U.P. ADAMEK MARIUSZ</h3>
							<p>Visual identification</p>
							<a class="capPreview image-modal" href="#image-modal3">preview</a>
						</div>
					</div>  
					<div id="image-modal3" class="mfp-hide modal-box">
                		<img src="assets/images/works/fhup-iv.jpg" alt="Logo Wizytowki Identyfikacja Wizualna Adamek Mariusz Hurtownia Pieczarek Pieczarki">
                  		<div class="modal-box-content">
                    		<h2><a href="http://adamekmariusz.pl">F. H. U. P. ADAMEK MARIUSZ</a></h2>
                    		<p>Identyfikacja wizualna zaprojektowana dla F. H. U. P. Adamek Mariusz, w której skład wchodzą: nowe logo marki wraz z przykładami jego 
								zastosowań oraz wizytówki firmowe. Założeniem projektu było stworzenie nowoczesnego, opartego na złotej proporcji logo. Kolory biały </br>
								i brązowy zostały wybrane ze względu na kolorystykę w jakiej występują pieczarki, złoty natomiast jest skojarzony z samochodami 
								firmowymi i odpowiednio kontrastuje z dobranym wcześniej brązem.</p>
                  		</div>
					</div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-12 worksItem worksCateg2">
					<img src="assets/images/works/mk-pl.jpg" alt="Plakat Typograficzny Mark Knopfler Tracker">
					<div class="itemCaption">
						<div class="capContent">
							<h3>MARK KNOPFLER "TRACKER"</h3>
							<p>Poster</p>
							<a class="capPreview image-modal" href="#image-modal4">preview</a>
						</div>
					</div>  
					<div id="image-modal4" class="mfp-hide modal-box">
                		<img src="assets/images/works/mk-pl.jpg" alt="Plakat Typograficzny Mark Knopfler Tracker">
                  		<div class="modal-box-content">
                    		<h2>MARK KNOPFLER "TRACKER"</h2>
                    		<p>Część pełnej identyfikacji wizualnej zaprojektowanej dla Marka Knopflera. Celem projektu była promocja trasy koncertowej oraz płyty „Tracker” przedstawiona w typograficznym stylu. Pierwszy z plakatów został wzbogacony o drobne elementy 		graficzne, aby dopełnić wizerunku gitary, która jest nieodłącznym symbolem Marka. Druga, oszczędniejsza w formę, wersja plakatu została ograniczona do czystej typografii. Litery są tu pocięte w struny i ukształtowane w gryf gitary. 		Założeniem wersji alternatywnej było zwiększenie czytelności przekazu informacji bez użycia elementów graficznych.</p>
                  		</div>
					</div>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12 worksItem worksCateg4">
					<img src="assets/images/works/zapro-ge.jpg" alt="Zaproszenie Chrzest Swiety">
					<div class="itemCaption">
						<div class="capContent">
							<h3>INVITATION</h3>
							<p>Editorial graphic</p>
							<a class="capPreview image-modal" href="#image-modal5">preview</a>
						</div>
					</div>  
					<div id="image-modal5" class="mfp-hide modal-box">
                		<img src="assets/images/works/zapro-ge.jpg" alt="Zaproszenie Chrzest Swiety">
                  		<div class="modal-box-content">
                    		<h2>INVITATION</h2>
                    		<p>Projekt dwustronnych zaproszeń na Chrzest Święty siostry, stworzony na użytek własny. Szkic Kościoła Mariackiego został odpowiednio przekoloryzowany i postarzony, tak aby dosadniej oddawał klimat bogatego w historię miasta Krakowa. 			Jednocześnie symbolizuje on tutaj miejsce uroczystego przyjęcia sakramentu.</p>
                  		</div>
                 	</div>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12 worksItem worksCateg2">
					<img src="assets/images/works/hhk-pl.jpg" alt="Plakat Afisz Lineup Hip Hop Kemp">
					<div class="itemCaption">
						<div class="capContent">
							<h3>HIP HOP KEMP 2016</h3>
							<p>Poster</p>
							<a class="capPreview image-modal" href="#image-modal6">preview</a>
						</div>
					</div>  
					<div id="image-modal6" class="mfp-hide modal-box">
                		<img src="assets/images/works/hhk-pl.jpg" alt="Plakat Afisz Lineup Hip Hop Kemp">
                  		<div class="modal-box-content">
                    		<h2>HIP HOP KEMP 2016</h2>
                    		<p>Alternatywna, odświeżona wersja plakatu promującego międzynarodowy festiwal HIP HOP KEMP 2016. Jest to pełna koncertów impreza kultury hip-hopowej, która corocznie w sierpniu odbywa się w Czechach. Założeniem projektu była czytelna i atrakcyjna prezentacja występujących na scenie artystów oraz oddanie klimatu masy odbywających się na miejscu koncertów z perspektywy widza.</br></br>
							Afisz, będący dopełnieniem plakatu, przedstawia tzw. „line-up”, czyli godzinowy plan rozłożonych na 3 dni koncertów.
							Założeniem projektu była spójność z zaprojektowanym wcześniej plakatem, czytelna i atrakcyjna wizualnie prezentacja treści oraz oddanie </br>
							klimatu masy odbywających się na miejscu koncertów, tym razem jednak z perspektywy wokalisty.</p>
                  		</div>
                	</div>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12 worksItem worksCateg2">
					<img src="assets/images/works/ddd-pl.jpg" alt="Plakat Piles Nie jedz Dont Drive Drunk">
					<div class="itemCaption">
						<div class="capContent">
							<h3>DON'T DRIVE DRUNK!</h3>
							<p>Poster</p>
							<a class="capPreview image-modal" href="#image-modal7">preview</a>
						</div>
					</div>  
					<div id="image-modal7" class="mfp-hide modal-box">
                		<img src="assets/images/works/ddd-pl.jpg" alt="Plakat Piles Nie jedz Dont Drive Drunk">
                  		<div class="modal-box-content">
                    		<h2>DON'T DRIVE DRUNK!</h2>
							<p>Plakat promujący kampanię „Piłeś? Nie jedź!”. Brał udział w Wystawie Plakatu Studenckiego w galerii Absurdalna w Katowicach. Grafika została tu zaprojektowana tak, 
								aby można było odkryć w dniej „drugie dno”. Po obróceniu obrazu o 180<sup>o</sup> widz odkrywa nową, drastyczną perspektywę, która symbolizuje efekty prowadzenia 
								pojazdu w stanie „po spożyciu” alkoholu.</p>
                  		</div>
					</div>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12 worksItem worksCateg4">
					<img src="assets/images/works/okladki-ge.jpg" alt="Okladki Seria Grafika Edytorska Rysunek Cienkopis Hamlet Makbet Romeo Julia">
					<div class="itemCaption">
						<div class="capContent">
							<h3>A SERIES OF THREE COVERS</h3>
							<p>Editorial graphic</p>
							<a class="capPreview image-modal" href="#image-modal8">preview</a>
						</div>
					</div>  
					<div id="image-modal8" class="mfp-hide modal-box">
                		<img src="assets/images/works/okladki-ge.jpg" alt="Okladki Okladka Seria Grafika Edytorska Rysunek Cienkopis Hamlet Makbet Romeo Julia">
                  		<div class="modal-box-content">
							<h2>A SERIES OF THREE COVERS</h2>
							<p>Cykl trzech stylistycznie spójnych okładek do klasyków Williama Shakespeare’a. Założeniem projektów było sporządzenie serii bezbarwnych ilustracji i przyznanie każdej z nich po jednym, 
							   adekwatnym kolorze dodatkowym, używanym zarówno w tytułach </br> jak i na grzbietach książek. Rysunki powstały przy użyciu cienkopisu   i nabrały kolorów na etapie edycji komputerowej.</p>
                  		</div>
                	</div>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12 worksItem worksCateg2">
					<img src="assets/images/works/cyfro-pl.jpg" alt="Plakat Wystawa Prac Student Studentow Studenci Cyfrowizacje">
					<div class="itemCaption">
						<div class="capContent">
							<h3>CYFROWIZACJE 2</h3>
							<p>Poster</p>
							<a class="capPreview image-modal" href="#image-modal9">preview</a>
						</div>
					</div>  
					<div id="image-modal9" class="mfp-hide modal-box">
                		<img src="assets/images/works/cyfro-pl.jpg" alt="Plakat Wystawa Prac Student Studentow Studenci Cyfrowizacje">
                  		<div class="modal-box-content">
                    		<h2>CYFROWIZACJE 2</h2>
                    		<p>Plakat promujący wystawę prac studentów WSTI pod tytułem „Cyfrowizacje 2”, która odbyła się w galerii Absurdalna w Katowicach. Tytuł wystawy i jej zawartość 
							   okazały się inspiracją do finalnej formy plakatu, która została nasycona efektami wielowymiarowości.</p>
                  		</div>
                	</div>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12 worksItem worksCateg3">
					<img src="assets/images/works/herb-pub.jpg" alt="Ksiazka Herb Lubalin Publikacja Typografia Kwadrat">
					<div class="itemCaption">
						<div class="capContent">
							<h3>HERB LUBALIN</h3>
							<p>Publication</p>
							<a class="capPreview image-modal" href="#image-modal10">preview</a>
						</div>
					</div>  
					<div id="image-modal10" class="mfp-hide modal-box">
                		<img src="assets/images/works/herb-pub.jpg" alt="Ksiazka Herb Lubalin Publikacja Typografia Kwadrat">
                  		<div class="modal-box-content">
                    		<h2>HERB LUBALIN</h2>
                    		<p>W pełni wydana książka o życiu oraz twórczości znakomitego amerykańskiego projektanta, jakim był Herb Lubalin. Designer </br>
								ten nadał literze zupełnie nową rolę w projektowaniu graficznym. Odrzucił tradycyjne zasady i rygory modernizmu, tworząc </br> 
								typografię ekspresywną. Manipulował w mistrzowski sposób formami liter i projektował obrazy na ich podstawie. To autor </br>
								genialnych, ponadczasowych logotypów, których część funkcjonuje po dzień dzisiejszy. Założeniem projektu była atrakcyjna </br>
								wizualnie, minimalistyczna i czysta prezentacja treści, godna zaprezentowania artysty doskonałego.</p>
						</div>
                	</div>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12 worksItem worksCateg4">
					<img src="assets/images/works/lvp-ge.jpg" alt="Grafika Edytorska Rysunek Cienkopis Ilustracja Las Vegas Parano">
					<div class="itemCaption">
						<div class="capContent">
							<h3>LAS VEGAS PARANO</h3>
							<p>Editorial graphic</p>
							<a class="capPreview image-modal" href="#image-modal11">preview</a>
						</div>
					</div>  
					<div id="image-modal11" class="mfp-hide modal-box">
                		<img src="assets/images/works/lvp-ge.jpg" alt="Grafika Edytorska Rysunek Cienkopis Ilustracja Las Vegas Parano">
                  		<div class="modal-box-content">
							<h2>LAS VEGAS PARANO</h2>
                    		<p>Cykl trzech ilustracji do pierwszego rozdziału książki, której oryginalny tytuł brzmi: „Fear and Loathing in Las Vegas: A Savage Journey to the Heart of the American Dream”. 
								Celem projektu było sporządzenie serii grafik przy użyciu czerni, bieli oraz czerwieni, jako koloru dodatkowego. Powstała ona przy użyciu cienkopisu oraz dwóch promarkerów. </p>
                  		</div>
                	</div>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12 worksItem worksCateg3">
					<img src="assets/images/works/futura-pub.jpg" alt="Ksiazka Publikacja Futura Promocja Kroju Typografia Kwadrat">
					<div class="itemCaption">
						<div class="capContent">
							<h3>FUTURA</h3>
							<p>Publication</p>
							<a class="capPreview image-modal" href="#image-modal12">preview</a>
						</div>
					</div>  
					<div id="image-modal12" class="mfp-hide modal-box">
                		<img src="assets/images/works/futura-pub.jpg" alt="Ksiazka Publikacja Futura Promocja Kroju Typografia Kwadrat">
                  		<div class="modal-box-content">
                    		<h2>FUTURA</h2>
                    		<p>W pełni wydana książka, która przedstawia, analizuje 
								i jednocześnie promuje krój pisma „Futura” autorstwa Paula Rennera. </br> Jest to godny uwagi, geometryczny, bezszeryfowy font zaprojektowany w latach 1924–26, 
								będący przejawem nowego podejścia </br> do projektowania. Dekoracyjne, zbędne elementy zostały w nim odrzucone na rzecz najważniejszych: funkcjonalizmu oraz formy. 
								Struktura i założenia „Futury” były inspiracją do layoutu wydanej publikacji.</p>
                  		</div>
					</div>
				</div>
			
				<div class="col-md-4 col-sm-4 col-xs-12 worksItem worksCateg3">
					<img src="assets/images/works/slownik-pub.jpg" alt="Slownik Angielski Polski Layout Design Publikacja Typografia">
					<div class="itemCaption">
						<div class="capContent">
							<h3>ENGLISH-POLISH DICTIONARY</h3>
							<p>Publication</p>
							<a class="capPreview image-modal" href="#image-modal13">preview</a>
						</div>
					</div>  
					<div id="image-modal13" class="mfp-hide modal-box">
                		<img src="assets/images/works/slownik-pub.jpg" alt="Slownik Angielski Polski Layout Design Publikacja Typografia">
                  		<div class="modal-box-content">
                    		<h2>ENGLISH-POLISH DICTIONARY</h2>
                    		<p>Typograficzna rozkładówka angielsko-polskiego słownika. Założeniem projektu była atrakcyjna wizualnie, czytelna forma przedstawienia tekstu z odpowiednimi zabiegami kolorystycznymi, 
								mającymi na celu przyciąganie wzroku użytkownika w odpowiednie miejsca, tak aby podnieść poziom prostoty użytkowania oraz szybkości odnajdywania pozycji.</p>
                  		</div>
                	</div>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12 worksItem worksCateg4">
					<img src="assets/images/works/wierszyki-pub.jpg" alt="Cykl Seria Ilustracji Ilustracje Wiersze Wierszyki Dla Dzieci Rysunek">
					<div class="itemCaption">
						<div class="capContent">
							<h3>ILLUSTRATED RHYMES</h3>
							<p>Editorial graphic</p>
							<a class="capPreview image-modal" href="#image-modal14">preview</a>
						</div>
					</div>  
					<div id="image-modal14" class="mfp-hide modal-box">
                		<img src="assets/images/works/wierszyki-pub.jpg" alt="Cykl Seria Ilustracji Ilustracje Wiersze Wierszyki Dla Dzieci Rysunek">
                  		<div class="modal-box-content">
                    		<h2>ILLUSTRATED RHYMES</h2>
                    		<p>Cykl odręcznych ilustracji do wybranych wierszyków autorstwa Tadeusza Śliwiaka oraz Ludwika Jerzego Kerna. Rysunki powstały przy pomocy ołówka i zostały pokolorwane promarkerami. 
								Następstwem było zeskanowanie ilustracji i zakomponowanie ich z tekstem.</p>
                  		</div>
            		</div>
				</div>
          
				<div class="col-md-4 col-sm-4 col-xs-12 worksItem worksCateg3">
					<img src="assets/images/works/mag-pub.jpg" alt="Artykul Rozkladowka Spread Magazyn Muzyka Publikacja Layout">
					<div class="itemCaption">
						<div class="capContent">
							<h3>ARTICLE IN MAGAZINE</h3>
							<p>Publication</p>
							<a class="capPreview image-modal" href="#image-modal15">preview</a>
						</div>
					</div>  
					<div id="image-modal15" class="mfp-hide modal-box">
                		<img src="assets/images/works/mag-pub.jpg" alt="Artykul Rozkladowka Spread Magazyn Muzyka Publikacja Layout">
                  		<div class="modal-box-content">
                   			<h2>ARTICLE IN MAGAZINE</h2>
                    		<p>Rozkładówka artykułu o tematyce muzycznej. Celem projektu było stworzenie layoutu i siatki magazynu muzycznego. </br> 
								Postawiłem tutaj na design nowoczesny, minimalistyczny i czytelny.</p>
                  		</div>
                	</div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-12 worksItem worksCateg5">
					<img src="assets/images/works/mbp-www.jpg" alt="Projekt Strona WWW Design Website Layout Miejska Biblioteka Publiczna Myslowice">
					<div class="itemCaption">
						<div class="capContent">
							<h3>THE CITY PUBLIC LIBRARY IN MYSŁOWICE</h3>
							<p>Website</p>
							<a class="capPreview image-modal" href="#image-modal16">preview</a>
						</div>
					</div>  
					<div id="image-modal16" class="mfp-hide modal-box">
                		<img src="assets/images/works/mbp-www.jpg" alt="Projekt Strona WWW Design Website Layout Miejska Biblioteka Publiczna Myslowice">
                  		<div class="modal-box-content">
                   			<h2>THE CITY PUBLIC LIBRARY IN MYSŁOWICE</h2>
                    		<p>Nowa wersja strony internetowej, zaprojektowanej dla Miejskiej Biblioteki Publicznej w Mysłowicach. Założeniem projektu była czytelna i atrakcyjna wizualnie prezentacja treści, 
								łatwy dostęp do sekcji oraz informacji kontaktowych, a także wygodna, szybka, podręczna wyszukiwarka książek w katalogach i zbiorach. Użytkownik, który wejdzie na stronę odczuwa 
								klimat pełnych półek z książkami, widzi fotel oraz łatwą do zauważenia wyszukiwarkę. Ma to na celu zainspirowanie go do znalezienia pozycji dla siebie.</p>
                  		</div>
                	</div>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12 worksItem worksCateg5">
					<img src="assets/images/works/piast-www.jpg" alt="Projekt Strona WWW Design Website Layout Grupa Piast Bedzin Gastronomia Sprzet Ciezki">
					<div class="itemCaption">
						<div class="capContent">
							<h3>G.P.U.H. PIAST S.A.</h3>
							<p>Website</p>
							<a class="capPreview image-modal" href="#image-modal17">preview</a>
						</div>
					</div>  
					<div id="image-modal17" class="mfp-hide modal-box">
                		<img src="assets/images/works/piast-www.jpg" alt="Projekt Strona WWW Design Website Layout Grupa Piast Bedzin Gastronomia Sprzet Ciezki">
                  		<div class="modal-box-content">
							<h2><a href="http://grupa-piast.pl">G. P. U. H. PIAST S.A.</a></h2>
                    		<p>Zrealizowane projekty nowoczesnych, responsywnych stron internetowych dla Górniczego Przedsiębiorstwa Usługowo-Handlowego „Piast” S. A. Grupa 
								ta dzieli swoją działalnosć między gastronomię, a sprzęt ciężki. Schemat strony obejmuje intro oraz wybór jednej </br> z powyższych sekcji. 
								Obie z nich zostały zaprojektowane na bazie jednego layoutu. Do napisania stron zostały użyte języki takie jak: HTML5, CSS3 oraz Javascript. </p>
                  		</div>
            		</div>
				</div>
          
				<div class="col-md-4 col-sm-4 col-xs-12 worksItem worksCateg5">
					<img src="assets/images/works/rzuc-www.jpg" alt="Projekt Strona WWW Design Website Layout Rzuc Palenie Kampania">
					<div class="itemCaption">
						<div class="capContent">
							<h3>QUIT SMOKING!</h3>
							<p>Website</p>
							<a class="capPreview image-modal" href="#image-modal18">preview</a>
						</div>
					</div>  
					<div id="image-modal18" class="mfp-hide modal-box">
                		<img src="assets/images/works/rzuc-www.jpg" alt="Projekt Strona WWW Design Website Layout Rzuc Palenie Kampania">
                  		<div class="modal-box-content">
                    		<h2>QUIT SMOKING!</h2>
                    		<p>Projekt responsywnej strony internetowej promującej kampanię „Rzuć palenie!” oraz serii grafik w niej zawartych. Celem strony była czytelna 
								i atrakcyjna wizualnie prezentacja treści oraz sformułowanie jej w taki sposób, aby była zachętą dla palacza do skończenia </br> z nałogiem. </p>
						</div>
             		</div>
				</div>

			</div>
		</div>
	</section>


    <!--section CONTACT -->
	<section id="sectionContact">
    	<div class="container">

        	<div class="sectionTitle wow fadeInUp">
        		<h1>Contact</h1>
        		<h4>Get in touch with me</h4>
        		<hr>
        	</div>

			<div class="contactForm wow fadeInDown">
				<div class="contactForm">
      
      				<div id="message"><?php echo $result; ?></div>
					
                    <form class="forma" method="post" action="#message">
						<input type="text" size="30" id="name" name="name" placeholder="First & Last Name" value="<?php echo htmlspecialchars($_POST['name']); ?>">
						<?php echo "<p class='text-danger'>$errName</p>";?>
						<input type="email" size="30" id="email" name="email" placeholder="Your E-mail Address" value="<?php echo htmlspecialchars($_POST['email']); ?>">
						<?php echo "<p class='text-danger'>$errEmail</p>";?>
						<input type="subject" size="30" id="subject" name="subject" placeholder="Subject" value="<?php echo htmlspecialchars($_POST['subject']); ?>">
						<?php echo "<p class='text-danger'>$errSubject</p>";?>
						<textarea cols="40" rows="3" name="message" placeholder="Message"><?php echo htmlspecialchars($_POST['message']);?></textarea>
						<?php echo "<p class='text-danger'>$errMessage</p>";?>
						<input type="text" size="30" id="human" name="human" placeholder="2 + 3 = ?">
						<?php echo "<p class='text-danger'>$errHuman</p>";?>
						<input id="submit" name="submit" type="submit" value="Send Message">
					</form> 

				</div>
			</div>
		</div>

      <!-- FOOTER -->
		<footer class="contactFooter">
			<div class="footer">
					© adamek.pro 2018 All rights reserved.
            		<a class="easing" href="#"><div class="fa fa-chevron-circle-up"></div></a>
        		</div>
			</div>
		</footer>
	</section>
	
    </div>
</div>
    
<!-- JAVASCRIPTS -->
<!--Main JS Library-->
<script src="assets/js/jquery-latest.js" type="text/javascript"></script>
<script src="assets/js/jquery.js"></script>
<!--Main Bootstrap JS-->
<script src="assets/js/bootstrap.js"></script>
<!--JS for easy scrolling-->
<script src="assets/js/jquery.easing.js"></script>
<!--JS for mixitup plugins, for works grid-->
<script src="assets/js/jquery.mixitup.js"></script>
<!--JS for MAGNIFIC POPUP & ALL SCRIPTS& CALLS-->
<script src="assets/js/scripts.js"></script>
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<!-- AJAX Form Submit -->
<script src="assets/js/contactform.js"></script>
<!-- Header fixed after scroll -->
<script src="assets/js/smoothscroll.js"></script>
<script src="assets/js/classie.js"></script>
<!-- OWL SLIDER & WOW ANIMATIONS -->
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/main.js"></script>

<script>
     new WOW().init();
</script>
</body>
</html>

